#!/bin/bash
# 20190102 : NeoX : publication vers gitlab
BASE="https://www.sia.aviation-civile.gouv.fr/schedules"
AZBA_FILE=$(curl -s $BASE -k | grep azba_lien | awk '{print $3}' | cut -d'"' -f2)
AZBA_BACKUPS_DIR=~/rtba_pdf_backups
OUTFOLDER=/var/www/html/azba
mkdir -p $OUTFOLDER

function found()
{
	if [[ ! $(which $1) ]]
	then
		echo "$1 missing, exiting and doing nothing"
		exit 1
	fi
	echo "$1 found"
	export PREREQUIS=1
}

if [ ! $PREREQUIS ]
then
	echo " Verification des pre-requis"
	found pdfgrep
	found wget
fi

case $1 in
	get_pdf)
		#rm -rf *.pdf
		mkdir -p $AZBA_BACKUPS_DIR
		mv *.pdf $AZBA_BACKUPS_DIR/
		for file in ${AZBA_FILE};do
			wget -q $file
		done
	;;
	raw)
		OUTPUT=${OUTFOLDER}/azba.raw
		SEP=";"
		# ouverture du fichier de destination
		echo "===" >$OUTPUT
		oldmessage=""

		# Parcours des PDF pour extraire les noms et activation des zones
		for file in ${AZBA_FILE};do
			source=$(basename $file)
			message=$(pdfgrep 'Message AZBA' $source)
			if [[ ${message} != ${oldmessage} ]]
			then
				#echo "Valide / Valid;Source from" >>$OUTPUT
				#echo $message";"$file >>$OUTPUT
				echo "Source ; $file" >>$OUTPUT
				echo "Valide / Valid;$message" >>$OUTPUT

				#pdfgrep 'ACTIVE' $source | sed -e 's@ @'$SEP'@g' | tr -s $SEP > $OUTPUT
				pdfgrep 'ACTIVE' $source | sed -e 's@ @_@' -e's@ @'$SEP'@g' | tr -s $SEP >> $OUTPUT
				echo "===" >>$OUTPUT
				
				oldmessage=$message
			fi

		done
		# Nettoyage de quelques zones dont le nom ne respecte pas la nomenclature
		sed -i -e 's@R69_'$SEP'CHAMPAGNE@R69_CHAMPAGNE@g' $OUTPUT
		sed -i -e 's@R56_'$SEP'LORIENT@R56_LORIENT@g' $OUTPUT
		sed -i -e 's@R57_'$SEP'BRETAGNE@R57_BRETAGNE@g' $OUTPUT
		sed -i -e 's@R142_'$SEP'NIEVRE@R142_NIEVRE@g' $OUTPUT
		sed -i -e 's@R143_'$SEP'AUVERGNE@R143_AUVERGNE@g' $OUTPUT
		sed -i -e 's@R144_'$SEP'LOIRE@R144_LOIRE@g' $OUTPUT
		sed -i -e 's@R152_'$SEP'ALSACE@R152_ALSACE@g' $OUTPUT
		sed -i -e 's@_'$SEP'@'$SEP'@' $OUTPUT
		sed -i -e 's@ACTIVE'$SEP'@ACTIVE,@g' $OUTPUT
		sed -i -e 's@ACTIVE,$@ACTIVE$@g' $OUTPUT
		
	;;
	csv|txt)
		# Selection du format de sortie
		case $1 in
			csv)
				OUTPUT=${OUTFOLDER}/azba.csv
				SEP=";"
			;;
			txt)
				OUTPUT=${OUTFOLDER}/azba.txt
				SEP="\t"
			;;
		esac
		# recuperation des données brutes
		cat ${OUTFOLDER}/azba.raw | sed -e 's/;/'$SEP'/g' >$OUTPUT


		# Insertion du pied de page
		echo "===" >>$OUTPUT
		echo " Recuperation faite le $(date)" >>$OUTPUT
		echo " ce script doit tourner a 8h et 20h ">>$OUTPUT
		echo " ce script est une demonstration technique et n'a pas de valeur legale">>$OUTPUT
		echo " Code source disponible ici : https://gitlab.com/neox/extraction-rtba-sia" >>$OUTPUT
		echo "===" >>$OUTPUT
		echo " Last run "$(date) >>$OUTPUT
		echo " this script should run at 8am and 8pm">>$OUTPUT
		echo " this page is just proof of concept and has no legal value">>$OUTPUT
		echo " Source code hosted here : https://gitlab.com/neox/extraction-rtba-sia" >>$OUTPUT
	;;
	html)
		OUTPUT=${OUTFOLDER}/azba.html

		# generation du code HTML
		echo "<html><head></head>">$OUTPUT
		echo "<table border=1>">>$OUTPUT
		echo "<tr><td>Recuperation faite / Last run at</td><td colspan=9><b>"$(date)"</b></td></tr>" >>$OUTPUT

		# Recuperation des données brutes et remplacement des ; par des colonnes de tableau
		head -n -10 ${OUTFOLDER}/azba.csv | sed -e 's@Message AZBA@<b>@' -e 's@UTC;@UTC;</b>@' -e 's@,@</br>@g' -e 's@^@<tr><td>@g' -e 's@;@</td><td>@g' -e 's@$@</td></tr>@g' >>$OUTPUT

		# Insertion du pied de page	
	        echo "<tr><td colspan=5>ce script doit tourner a 8h et 20h</td><td colspan=5>this script should run at 8am and 8pm</td></tr>">>$OUTPUT
	        echo "<tr><td colspan=5>ce script est une demonstration technique et n'a aucune valeur legale</td><td colspan=5>this page is just proof of concept and has no legal value</td></tr>">>$OUTPUT
		echo "<tr><td colspan=5>Code source du projet :</td><td colspan=5>Source code is hosted here :</td></tr>">>$OUTPUT
		echo "<tr><td colspan=10><span align=center><a href="https://gitlab.com/neox/extraction-rtba-sia">https://gitlab.com/neox/extraction-rtba-sia</a></span></td></tr>">>$OUTPUT
		echo "<tr><td colspan=9>Source de données / Datasource</td><td>$BASE</td></tr>" >>$OUTPUT
		echo "</table>" >> $OUTPUT
		echo "</html>" >>$OUTPUT

		# Nettoyage 
		sed -i -e 's#<tr><td>===</td></tr>#<tr><td colspan=10><hr /></td></tr>#g' $OUTPUT

		# Activation du code couleur
		case $# in 
			1)
			;;
			2)
				case $2 in
					color )
						sed -i -e 's#:ACTIVE#<font color=red>:ACTIVE</font>#g' $OUTPUT
						sed -i -e 's#NONACTIVE#<font color=green>NONACTIVE</font>#g' $OUTPUT
					;;
				esac
			;;
		esac
	;;
	json)
		echo "$1 is coming soon"
	;;
	all)
		$0 get_pdf
		$0 raw
		$0 csv
		$0 txt
		$0 html color
	;;
	*)
		echo "usage :"
		echo "to get all pdf and generate all format \n\t $0 <all>"	
		echo "to refresh output without refresh pdf source \n\t$0 <csv|txt|htmljson> [color]"	
		echo " use color to beautify your HTML"
	;;
esac

