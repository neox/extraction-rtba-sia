Ce script prend ses infos sur le SIA
https://www.sia.aviation-civile.gouv.fr/schedules

Il recupere tous les PDF fournis et les analyse pour en recuperer les zones et leurs activations (ou absence d'activation)

Il en fait un rendu au format txt et html
et pour le HTML dispose d'option couleur (active par defaut)

une demonstation est visible ici
http://demo1.gorlier.net/azba

Utilisation : 

Pour avoir TOUS les fichiers PDF du SIA et generer les html et txt
./get_azba.sh <all>

L'option **all** ne fait que jouer le script plusieurs fois avec les options suivantes :

./get_azba.sh get_pdf (pour recuperer les PDF, les anciens PDF sont sauvegardés dans un dossier du home utilisateur lors du lancement de cette etape)

./get_azba.sh raw (pour lire les PDF et en extraire les noms et horaires d'activation, sortie vers le fichier .raw)

./get_azba.sh txt (pour calculer la version texte a partir de la version raw)

./get_azba.sh csv (pour calculer la version csv a partir de la version raw)

./get_azba.sh html color (pour calculer la version HTML avec les couleurs a partir de la version raw)

Si vous ne souhaitez pas avoir les couleurs il suffit de supprimer la variable color lors de l'appel
./get_azba.sh html
est un appel valide, qui fera un fichier html qui restera en noir et blanc

Pour ne generer que les sorties à partir de PDF deja existants (si vous retouchez le script par exemple)
./get_azba.sh <html|txt> [color]
